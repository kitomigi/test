
import json
from time import sleep
import socket
import requests
from flask import Flask

app = Flask(__name__)

BASE_CONSUL_URL = 'http://consul:8500'
# BASE_CONSUL_URL = 'http://127.0.0.1:8501'

SERVICE_ADDRESS = socket.gethostbyname(socket.gethostname())
HOSTNAME_APP = socket.gethostname()
PORT = 8080
#print = SERVICE_ADDRESS

@app.route('/')
def home():
    # return 'Hello World from {address}'.format(address=SERVICE_ADDRESS)
    # return 'My hostname{address}'.format(address=hostname)
    html = "<h3>Hello world!</h3>""<b>Hostname: {hostname}</b><br/>""<b>IP: {ip}</b> : {port_find}<br/>"

    #return html.format(name=os.getenv("NAME", "World"), hostname=HOSTNAME_APP, ip=SERVICE_ADDRESS)
    return html.format(hostname=HOSTNAME_APP, ip=SERVICE_ADDRESS, port_find=find_free_port)




@app.route('/health')
def hello_world():
    data = {
        'status': 'healthy'
    }
    return json.dumps(data)

@app.route('/register')
def register():
    url = BASE_CONSUL_URL + '/v1/agent/service/register'
    data = {
        'Name': HOSTNAME_APP,
        #'Tags': ['flask'],
        'Address': SERVICE_ADDRESS,
        # 'Address': 'http://127.0.0.0.1:8080',
        'Port': PORT,
        'Check': {
            'http': 'http://{address}:{port}/health'.format(address=SERVICE_ADDRESS, port=PORT),
            # 'http': 'http://127.0.0.1:{port}/health'.format(port=PORT),
            'interval': '10s'
        }
    }
    app.logger.debug('Service registration parameters: ', data)
    res = requests.put(
        url,
        data=json.dumps(data)
    )
    return res.text

def find_free_port():
    s = socket.socket()
    s.bind(('', 0))            # Bind to a free port provided by the host.
    return s.getsockname()[1]  # Return the port number assigned.

if __name__ == '__main__':
    sleep(8)
    try:
        app.logger.debug(register())
    except:
        app.logger.debug('Something wrong happened!')
        pass
    app.run(debug=True, host="0.0.0.0", port=PORT)