"/bin/sh"
# -*- coding: utf8 -*-
from sanic import Sanic
from sanic.response import json
import requests
import os
app = Sanic()

"""
APP1
"""
# app1_host = os.getenv('APP1_HOST', '0.0.0.0')
# app1_port = os.getenv('APP1_PORT', 8000)

# app2_host = os.getenv('APP2_HOST', '0.0.0.0')
# app2_port = os.getenv('APP2_PORT', 8001)


app1_host = os.getenv('APP1_HOST')
app1_port = os.getenv('APP1_PORT')

app2_host = os.getenv('APP2_HOST')
app2_port = os.getenv('APP2_PORT')

@app.route('/')
async def test1(request):
    return json({'me app1 - hello': 'world'}, headers={'X-Served-By': 'sanic'})

@app.route('/app2')
async def test2(request):
    app1 = requests.get(f"http://{app2_host}:{app2_port}/").json()
    return json({'me app1 - hello': 'world', 'data': app1}, headers={'X-Served-By': 'sanic'})


if __name__ == '__main__':
    app.run(host=app1_host, port=app1_port, auto_reload=True)
