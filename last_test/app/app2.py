from sanic import Sanic
from sanic.response import json
import requests
import os


app = Sanic()


"""
APP2
"""
app1_host = os.getenv('APP1_HOST')
app1_port = os.getenv('APP1_PORT')

app2_host = os.getenv('APP2_HOST')
app2_port = os.getenv('APP2_PORT')



@app.route('/')
async def test1(request):
    return json({'me app2 - hello': 'world'}, headers={'X-Served-By': 'sanic'})

@app.route('/app1')
async def test2(request):
    app2 = requests.get(f"http://{app1_host}:{app1_port}/").json()
    return json({'me app2 - hello': 'world', 'data': app2}, headers={'X-Served-By': 'sanic'})





if __name__ == '__main__':
    app.run(host=app2_host, port=app2_port, auto_reload=True)
