if [ "$(docker network ls -q -f name=web-side)" ]; then 

    # run your container
    echo "###############################"
    echo "#    You have a network       #"
    echo "###############################"
fi
if [ ! "$(docker network ls -q -f name=web-side)" ]; then
    # cleanup
    docker network create web-side 
        echo "###############################"
        echo "#    Network create           #"
        echo "###############################"
fi 