###  Это "болванка" для дальнейшей разработки. 
Для проверки обязательно должен быть установлен docker и docker-compose.

В проекте прописаны след хосты 

<http://traefik.docker.localhost>

<http://consul.docker.localhost>

<http://portainer.docker.localhost>

<http://app1.docker.localhost>

<http://app2.docker.localhost>



Для начало запустите **`make up_infra`** это создаст инфраструктуру проекта 

<http://traefik.docker.localhost>

<http://consul.docker.localhost>

<http://portainer.docker.localhost>

**`Проверьте запуск проекта зайдя на по хостам выше`**

После запустите APP **`make upd_app`**

Для обновления кода в контейнере используйте **`make upd_app`**

Остановить проект **`make stop_app`**

Остановить инфраструктуру **`make stop_infra`**