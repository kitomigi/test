#include .env
SHELL := /bin/bash 
up_infra:
	./network.sh && docker-compose -f traefik.yml up -d && docker-compose -f consul.yml up -d && docker-compose -f portainer.yml up -d 
up_app:
	docker-compose -f app2.yml up -d
up_db:
	docker-compose -f db.yml up -d


upd_app:
	docker-compose -f app2.yml stop && docker-compose -f app2.yml up -d --force-recreate --build
# upb:
# 	docker-compose up -d --force-recreate --build


stop:
	docker-compose -f traefik.yml stop && docker-compose -f consul.yml stop && docker-compose -f app2.yml stop && docker-compose -f portainer.yml stop
stop_app:
	docker-compose -f app2.yml stop
stop_db:
	docker-compose -f db.yml stop


down_infra:
	docker-compose -f traefik.yml stop && docker-compose -f consul.yml stop && docker-compose -f portainer.yml stop  && docker-compose -f traefik.yml down && docker-compose -f consul.yml down && docker-compose -f portainer.yml down
down_app:
	docker-compose -f app2.yml stop && docker-compose -f app2.yml down
down_db:
	docker-compose -f db.yml stop && docker-compose -f db.yml down

down_all:
	make down_db && make down_app && make down_infra  
# db:
# 	export PGPASSWORD=${POSTGRES_PASSWORD}; docker exec -it test_db psql -U $(POSTGRES_USER) ${POSTGRES_DB}
# r:
# 	docker exec -it test_redis  /usr/local/bin/redis-cli
# test:
# 	docker exec -it test_api pytest 
# b:
# 	docker exec -it $(c) /bin/bash